"""
Student Model
---------------------------
Contains a Student class derived from a base Person class
"""

from models.person import Person

class Student(Person):
  """
  Class to represent a Student
  Derived from Person Class
  """
  def __init__(self, fname, lname, year):
    super().__init__(fname, lname)
    self.graduationyear = year

  def welcome(self):
    """
    Prints a welcome statement to the console
    """
    print(
      "Welcome",
      self.firstname,
      self.lastname,
      "to the class of",
      self.graduationyear
    )
