"""
Person Model
---------------------------
Contains a base Person class
"""

class Person:
  """
  Class to represent a Person
  """
  def __init__(self, fname, lname):
    """ inits Person with data

    :param fname: Firstname of the person
    :param lname: Lastname of the person
    """
    self.firstname = fname
    self.lastname = lname

  def printname(self):
    """
    Prints a person's fullname to the console
    """
    print(self.firstname, self.lastname)
