### Run pylint
```bash
$ pylint <filename or directoryname>
```

### Generate docs
```bash
$ cd docs
$ make html
```