Welcome to Project-PY's documentation!
======================================

.. automodule:: models.person
    :members:

.. automodule:: models.student
    :members:


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
